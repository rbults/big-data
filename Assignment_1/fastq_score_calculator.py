#!/usr/bin/env/python3

"""
average phred score calculator.
This script calculates the average per base phred score, given a fastq file.

Author: Rene Bults
Version: 1.0
"""

import sys
import multiprocessing
import time
import os
import numpy as np


def equalize_arrays(array1, array2):
    """
    This function equalizes the arrays, if the previous score-line
    is larger or smaller than the current score-line
    """

    difference = array1.shape[0] - array2.shape[0]  # calculate difference in shapes

    if difference > 0:  # if the previous line is larger
        array2 = np.concatenate([array2, np.zeros((abs(difference), 1))])   # add difference to array in zeros

    elif difference < 0:    # if the previous line is smaller
        array1 = np.concatenate([array1, np.zeros((abs(difference), 1))])   # add difference to array in zeros

    return array1, array2


def calculate_scores(data, chunk, result_q, end_of_part):
    """
    This function calculates the average phred score for every read
    """

    with open(data) as fastq:
        fastq.seek(chunk, 0)
        fastq.readline()
        start_reading = False
        counter = 1
        end = False
        scores = np.zeros((1, 1))
        nums = np.zeros((1, 1))

        while True:
            line = fastq.readline()
            line = line.strip()
            if line.startswith("@D"):
                if len(line) > 60:
                    fastq.readline()

                start_reading = True

            if start_reading:
                if (counter % 4) == 0:  # loop over every read
                    new_scores = np.asarray(list(bytes(line, 'ascii')))         # calculate ordinal value current line
                    new_scores = new_scores.reshape(new_scores.shape[0], 1)     # reshape for division of arrays

                    new_nums = np.asarray([1] * len(line))  # create line-sized array with a counter for every line
                    new_nums = new_nums.reshape(new_nums.shape[0], 1)           # reshape for division of arrays

                    scores, new_scores = equalize_arrays(scores, new_scores)   # function is functional only when needed
                    nums, new_nums = equalize_arrays(nums, new_nums)     # function is functional only when needed

                    scores += new_scores    # add new_scores to scores, creating a sum of the scores
                    nums += new_nums    # add new_nums to scores, creating a sum of the nums

                counter += 1

                if fastq.tell() >= end_of_part:  # if the end of the part is reached
                    end = True
                    break

            if not line:    # if there is no line left to read
                break

        if end:
            result_q.put(list(scores / nums))   # put results of current part in the queue
            return 0


def worker():
    """
    This function creates the different processes and writed
    the output to a results file
    """

    filesize = os.path.getsize(sys.argv[1])
    chunk = filesize / int(sys.argv[3])
    result_q = multiprocessing.Queue()
    next_chunk = 0
    queue_combined = []
    final_scores = []
    processes = []

    for i in range(int(sys.argv[3])):
        new_process = multiprocessing.Process(
            target=calculate_scores,
            args=((sys.argv[1]), next_chunk, result_q, next_chunk + chunk))

        next_chunk += chunk
        processes.append(new_process)
        new_process.start()

    # Join all processes
    for i in processes:
        i.join()

    # Combine the results of all jobs to one big list
    for i in range(result_q.qsize()):
        queue_combined.append(result_q.get())

    # Add these lists together into one list
    for item in zip(*queue_combined):
        final_scores.append(sum(item) / int(sys.argv[3]) - 33)

    # Write the output to a new file
    with open(str(sys.argv[4]), "w") as output:
        for item in final_scores:
            output.write(str(item))
            output.write("\n")


def main():
    start = time.time()
    worker()
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())
