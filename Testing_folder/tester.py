#!/usr/bin/env/python3

"""
average phred score calculator.
This script calculates the average per base phred score, given a fastq file.

Author: Rene Bults
Version: 1.0
"""

import sys
import multiprocessing
import time
import os
import numpy as np


def equalize_arrays(array1, array2):
    difference = array1.shape[0] - array2.shape[0]
    if difference > 0:
        array2 = np.concatenate(
            [array2, np.zeros((abs(difference), 1))]
        )
    elif difference < 0:
        array1 = np.concatenate(
            [array1, np.zeros((abs(difference), 1))]
        )
    return array1, array2


# Calculate the score of every nucleotide
def calculate_scores(data):
    with open(data) as fastq:

        #fastq.readline()
        start_reading = False
        counter = 1
        end = False
        scores = np.zeros((1, 1))
        nums = np.zeros((1, 1))

        while True:
            line = fastq.readline()
            if line.startswith("@D"):
                if len(line) > 60:  # check if the length of the first @ is longer than the second. this to skip line if the score-line starts with @
                    fastq.readline()

                start_reading = True  # wordt op 'True' gezet en komt niet meer van True af in deze loop;

            if start_reading:
                if (counter % 4) == 0:  # loop over every read
                    new_scores = np.asarray(list(bytes(line, 'ascii'))) - 32
                    new_scores = new_scores.reshape(new_scores.shape[0], 1)

                    new_nums = np.asarray([1] * len(line))
                    new_nums = new_nums.reshape(new_nums.shape[0], 1)

                    scores, new_scores = equalize_arrays(scores, new_scores)
                    nums, new_nums = equalize_arrays(nums, new_nums)

                    scores += new_scores
                    nums += new_nums

                counter += 1

            if not line:
                break

        print(list(scores / nums))
        #result_q.put(list(scores / nums))

        # put result into queue for every process after the end is reached




def main():
    start = time.time()
    calculate_scores(sys.argv[1])
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())
