# Big Data

## Information
This subject is for the optimization of heavy calculations by running them on multiple nodes
or processes. The goal is to create an average per-base phred-score calculator, which runs on multiple
nodes or processes. The different approaches are explained below.

### Usage
Assignment 1 and 2 should be run like this: `python3 script.py <fastq> -n <cores> <output.csv>`

Assignment 3 should be run like this: `./perigrine.sh`
            
### Assignment 1
Run the calculator on multiple processes on one machine

### Assignment 2
Run the calculator on multiple computers on the bio-informatics network

### Assignment 3
Run the calculator of assignment 1 on the peregrine cluster of the RUG

### Assignment 4
Run the calculator on multiple nodes of the peregrine cluster of the RUG

