#!/usr/bin/env/python3

"""
Peregrine script. Calculates the average phred score using the perigrine
cluster, with multiple nodes, which are assigned randomly.

Author: Rene Bults
Version: 0.1a
"""

import socket
import time
import sys


def create_client():
    host = "127.0.0.1"
    port = 5001

    server = ("127.0.0.1", 5000)

    socket_c = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    socket_c.bind((host, port))

    message = input("-> ")
    while message != "q":
        socket_c.sendto(message.encode(), server)
        data, address = socket_c.recvfrom(1024)
        print("received back from server:", str(data))
        message = input("-> ")

    socket_c.close()

    return socket_c


def main():
    start = time.time()
    create_client()
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())