#!/usr/bin/env/python3

"""
Peregrine script. Calculates the average phred score using the perigrine
cluster, with multiple nodes, which are assigned randomly.

Author: Rene Bults
Version: 0.1a
"""

import socket
import time
import sys


def create_server():
    host = "127.0.0.1"
    port = 5000

    socket_m = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    socket_m.bind((host, port))

    print("Server started")
    while True:
        data, address = socket_m.recvfrom(1024)
        print("message from:", str(address))
        print("message is:", str(data))

        data = str(data) + str(" I was sent back")
        print("sending back to client:", str(data))
        socket_m.sendto(data.encode(), address)


    socket_m.close()

    return socket_m


def main():
    start = time.time()
    create_server()
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())