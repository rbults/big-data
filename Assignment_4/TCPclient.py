#!/usr/bin/env/python3

"""
Peregrine script
"""

import socket
import time
import sys


def create_client():
    host = "127.0.0.1"
    port = 5000

    socket_c = socket.socket()
    socket_c.connect((host, port))

    message = input("-> ")

    while message != "q":
        socket_c.send(message.encode())
        data = socket_c.recv(1024)
        print("received from server:", str(data))
        message = input("-> ")

    socket_c.close()

    return socket_c


def main():
    start = time.time()
    create_client()
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())