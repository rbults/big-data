#!/usr/bin/env/python3

"""
Peregrine script
"""

import socket
import time
import sys


def create_server():
    host = "127.0.0.1"
    port = 5000

    socket_m = socket.socket()
    socket_m.bind((host, port))
    socket_m.listen(1)

    c, addr = socket_m.accept()
    print("connection from", str(addr))

    while True:
        data = c.recv(1024)
        if not data:
            break

        print("from connected user:", str(data))
        data = str(data).upper()
        print("sending", str(data))
        c.send(data.encode())
    socket_m.close()

    return socket_m


def main():
    start = time.time()
    create_server()
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())