#!/usr/bin/env/python3

"""
average phred score calculator.
This script calculates the average per base phred score, given a fastq file.

This script does the same as assignment 1, but this script creates a master and
four other clients, who work on the job. This is done on different computers.

This is the client script.

This script should be run on four different terminal sessions, these are
the four clients who do the total job.

Author: Rene Bults
Version: 1.0
"""

import sys
from multiprocessing.managers import SyncManager
import multiprocessing
import numpy as np
import time
from queue import Empty


def equalize_arrays(array1, array2):
    """
    This function equalizes the arrays, if the previous score-line
    is larger or smaller than the current score-line
    """

    difference = array1.shape[0] - array2.shape[0]  # calculate difference in shapes

    if difference > 0:  # if the previous line is larger
        array2 = np.concatenate([array2, np.zeros((abs(difference), 1))])  # add difference to array in zeros

    elif difference < 0:  # if the previous line is smaller
        array1 = np.concatenate([array1, np.zeros((abs(difference), 1))])  # add difference to array in zeros

    return array1, array2


def calculate_scores(data, job_q, result_q):
    """
    This function calculates the average phred score for every read
    """

    try:
        chunk, end_of_part = job_q.get()
        with open(data) as fastq:
            fastq.seek(chunk)
            fastq.readline()
            start_reading = False
            counter = 1
            end = False
            scores = np.zeros((1, 1))
            nums = np.zeros((1, 1))

            while True:
                line = fastq.readline()
                if line.startswith("@D"):
                    if len(line) > 60:
                        fastq.readline()

                    start_reading = True

                if start_reading:
                    if (counter % 4) == 0:  # loop over every read
                        new_scores = np.asarray(list(bytes(line, 'ascii')))  # calculate ordinal value current line
                        new_scores = new_scores.reshape(new_scores.shape[0], 1)  # reshape for division of arrays

                        new_nums = np.asarray(
                            [1] * len(line))  # create line-sized array with a counter for every line
                        new_nums = new_nums.reshape(new_nums.shape[0], 1)  # reshape for division of arrays

                        scores, new_scores = equalize_arrays(scores,
                                                             new_scores)  # function is functional only when needed
                        nums, new_nums = equalize_arrays(nums, new_nums)  # function is functional only when needed

                        scores += new_scores  # add new_scores to scores, creating a sum of the scores
                        nums += new_nums  # add new_nums to scores, creating a sum of the nums

                    counter += 1

                    if fastq.tell() >= end_of_part:  # if the end of the part is reached
                        end = True
                        break

                if not line:  # if there is no line left to read
                    break

            if end:
                result_q.put(list(scores / nums))  # put results of current part in the queue
                return 0

    except Empty:
        print("empty queue")


def worker(shared_job_q, shared_result_q, file):
    """
    This function creates the different processes and writed
    the output to a results file
    """

    new_process = multiprocessing.Process(target=calculate_scores, args=(file, shared_job_q, shared_result_q))
    new_process.start()     # start process
    new_process.join()      # join process


def make_client_manager(ip, port, key):
    class ServerQueueManager(SyncManager):
        pass

    # register both queues
    ServerQueueManager.register('get_job_q')
    ServerQueueManager.register('get_result_q')

    # create a manager and connect to the server of the master
    manager = ServerQueueManager(address=(ip, port), authkey=key)
    manager.connect()

    print("connected to: {0}, on port: {1}".format(ip, port))

    return manager


def run_client():
    manager = make_client_manager('bin300', 5000, b'test')
    job_q = manager.get_job_q()
    result_q = manager.get_result_q()
    worker(job_q, result_q, sys.argv[1])


def main():
    start = time.time()
    run_client()
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())


