#!/usr/bin/env/python3

"""
average phred score calculator.
This script calculates the average per base phred score, given a fastq file.

This script does the same as assignment 1, but this script creates a master and
four other clients, who work on the job. This is done on different computers.

This is the master script.

Author: Rene Bults
Version: 1.0
"""


import sys
from multiprocessing.managers import SyncManager
import multiprocessing
import time
import os


def make_server_manager(port, key):
    job_q = multiprocessing.Queue()
    result_q = multiprocessing.Queue()

    class JobQueueManager(SyncManager):
        pass

    # register both queues
    JobQueueManager.register('get_job_q', callable=lambda: job_q)
    JobQueueManager.register('get_result_q', callable=lambda: result_q)

    # start a manager and start the server
    manager = JobQueueManager(address=('', port), authkey=key)
    manager.start()

    print("connected the master")

    return manager


def run_server():
    manager = make_server_manager(5000, b'test')     # random port and random key
    shared_job_q = manager.get_job_q()
    shared_result_q = manager.get_result_q()

    # create job parts for every client, and put them into the job_q
    filesize = os.path.getsize(sys.argv[1])
    next_chunk = 0
    chunk = filesize / int(sys.argv[3])
    for item in range(0, filesize, int(chunk)):
        shared_job_q.put((next_chunk, next_chunk + chunk))
        next_chunk += chunk

    # every time a client is done, 1 is added to count
    count = 0
    temp = []
    while count < (int(sys.argv[3])):
        result = shared_result_q.get()
        temp.append(result)
        count += 1

    # add all results from the clients together in one list
    final_scores = []
    for item in zip(*temp):
        final_scores.append(sum(item) / int(sys.argv[3]) - 33)

    # Write the output to a new file
    with open(str(sys.argv[4]), "w") as output:
        for item in final_scores:
            output.write(str(item))
            output.write("\n")

    time.sleep(2)   # sleep a bit to make sure the queue knows it is empty and shuts down
    manager.shutdown()  # close the server


def main():
    start = time.time()
    run_server()
    end = time.time()
    print("Processing time: ", end - start, "seconds")


if __name__ == '__main__':
    sys.exit(main())
