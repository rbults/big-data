# Shell script for running the script on the perigrine network

#!/bin/bash
#SBATCH --time 2:00:00      # estimated time for the program to run
#SBATCH --nodes=1           # amount of nodes to run on
#SBATCH --cpus-per-task=16  # amount of processors to do the job
#SBATCH --partition=short   # choice of partition type
#SBATCH --mem=5000          # memory size for the job to be run with

module load protobuf-python/3.4.0-foss-2016a-Python-3.5.2   # load the python module

python3 fastq_score_calculator.py ../rnaseq.fastq -n 4 results.csv  # execute the job on perigrine network
